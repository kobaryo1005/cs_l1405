#include "spinor_mat.h"

TComplex Amp_t_K(double Eg, double costh, // Eg: lab, costh: cm
    int hel_gamma, int mi, int mf) {
  TLorentzVector k(0, 0, Eg, Eg), p(0, 0, 0, Mp);  // lab system
  TLorentzVector W = k + p;
  /* lab --> CM */
  k.Boost(-W.BoostVector());
  p.Boost(-W.BoostVector());

  /* momentum for K+ */
  TLorentzVector q;
  double m3 = Mkaon;//charged kaon mass
  double m4 = Mlambdastar;//Lambda(1405) mass
  double pdk = TMath::Sqrt((W.M2()-sq(m3+m4))*(W.M2()-sq(m4-m3)))/(2*W.M()); 
  q.SetXYZM(pdk*TMath::Sqrt(1-sq(costh)), 0, pdk*costh, m3);

  // /* CM --> V rest frame */
  // k.Boost(-q.BoostVector());
  // p.Boost(-q.BoostVector());
  // q.Boost(-q.BoostVector());

  TLorentzVector pp = k + p - q; // mom4 in lab
  double s = W.M2();
  double t = (k-q).M2();

  /* polarization vector */
  TComplex eps_gamma[4];
  FillPolVector(k, hel_gamma, eps_gamma);

  /* deal with the difference of metric definition */
  double kk[4] = {k(3), k(0), k(1), k(2)};
  double qq[4] = {q(3), q(0), q(1), q(2)};
  double tt[4] = {k(3)-q(3), k(0)-q(0), k(1)-q(1), k(2)-q(2)};

  /* amplitude calculation */
  TComplex amp = TComplex(0,0);
  Mat matrixElement_ij;
  TComplex A_ij;
  // double g_KNLambdastar = 0.; 
  double g_KNLambdastar = 3.18; 
  TComplex A, B;
  A = 2. * charge * g_KNLambdastar;
B = 0;
  for (int i = 0; i < 4; i++) {
    B += qq[i] * g[i][i] *eps_gamma[i]; 
  }
  //Form Factor
  // double lambda_h = 0.650;//GeV
  double lambda_h = 0.650;//GeV
  double lambda_EM = 0.650;//GeV (300 MeV)
  double sq_mom_kaon = sq(tt[1]) + sq(tt[2]) + sq(tt[3]); 
  double sq_mom_gamma = sq(kk[1]) + sq(kk[2])+ sq(kk[3]); 
  double F_KNLambdastar = (sq(lambda_h)-sq(Mkaon))/(sq(lambda_h) + sq_mom_kaon);
  double F_gammaKK = sq(lambda_EM) / (sq(lambda_EM) + sq_mom_gamma);
  // std::cout << F_KNLambdastar * F_gammaKK << endl;
  // amp = A * B * barDot(u(pp, mf),u(p, mi)) * F_KNLambda * F_gammaKK;
  amp =TComplex(0, -1. * A * (1./(t - sq(Mkaon)) * B) * barDot(u(pp, mf),u(p, mi)) * F_KNLambdastar * F_gammaKK);
  return amp;
}





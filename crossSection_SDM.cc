#include "Amp_s.cc"
#include "Amp_u.cc"
#include "Amp_t_K.cc"
#include "Amp_t_Kstar.cc"

// costh in CM sys. and Eg in lab. sys.
//double dsigma_dt(double Eg, double costh) {
double dsigma_dt(double Eg, double costh) {
  TComplex I;
  double sum = 0.;  
  for (int mi=-1; mi<=+1; mi+=2) {
    for (int mf=-1; mf<=+1; mf+=2) {
      for (int hel_g=-1; hel_g<=+1; hel_g+=2) {
        I = TComplex(0,0);
        I += Amp_t_K(Eg, costh, hel_g, mi, mf);
        // I += Amp_t_Kstar(Eg, costh, hel_g, mi, mf);
        I += Amp_s(Eg, costh, hel_g, mi, mf);
        // I += Amp_u(Eg, costh, hel_g, mi, mf);
        sum += sq(TComplex::Abs(I));
      }
    }          
  }
double den = 64*pi*sq(2*Eg*Mp);
return (hbarc2*sum/den);  
}


double dsigma_dcosth(double Eg, double costh) { //costh in cm.
  double m1 = 0., m2 = Mp, m3 = Mkaon, m4 = Mlambdastar;
  double s = sq(Eg + Mp) - sq(Eg); // s is calculated in Lab system.
  double lambda12 = sq(s-sq(m1)-sq(m2)) - 4*sq(m1)*sq(m2);
  double lambda34 = sq(s-sq(m3)-sq(m4)) - 4*sq(m3)*sq(m4);

  double dt2dcosth = sqrt(lambda12 * lambda34) / (2*s);

  return dsigma_dt(Eg, costh) * dt2dcosth;
}


double dsigma(double Eg) {
  double n = 19.; 
  double min = -1., max = 1.;
  double dcosth = (max - min) / n;

  double sigma = 0;
  for (int i = 0; i < n; i++) {
    double costh = min + dcosth * (i + 0.5);
    sigma += dsigma_dcosth(Eg, costh) * dcosth;
  }

  return sigma;
}


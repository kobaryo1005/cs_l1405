#include "spinor_mat.h"

TComplex Amp_t_Kstar(double Eg, double costh, // Eg: lab, costh: cm
    int hel_gamma, int mi, int mf) {
  TLorentzVector k(0, 0, Eg, Eg), p(0, 0, 0, Mp);  // lab system
  TLorentzVector W = k + p;
  /* lab --> CM */
  k.Boost(-W.BoostVector());
  p.Boost(-W.BoostVector());

  /* momentum for K+ */
  TLorentzVector q;
  double m3 = Mkaon;//charged kaon mass
  double m4 = Mlambdastar;//Lambda(1405) mass
  double pdk = TMath::Sqrt((W.M2()-sq(m3+m4))*(W.M2()-sq(m4-m3)))/(2*W.M()); 
  q.SetXYZM(pdk*TMath::Sqrt(1-sq(costh)), 0, pdk*costh, m3);

  /* CM --> V rest frame */
  // k.Boost(-q.BoostVector());
  // p.Boost(-q.BoostVector());
  // q.Boost(-q.BoostVector());

  TLorentzVector pp = k + p - q; 
  double s = W.M2();
  double t = (k-q).M2();

  /* polarization vector */
  TComplex eps_gamma[4];
  FillPolVector(k, hel_gamma, eps_gamma);
  /* deal with the difference of metric definition */
  double kk[4] = {k(3), k(0), k(1), k(2)};
  double qq[4] = {q(3), q(0), q(1), q(2)};
  double tt[4] = {k(3)-q(3), k(0)-q(0), k(1)-q(1), k(2)-q(2)};

  /* coupling constant */
  double g_gammaKKstar = 0.254;//GeV^-1
  double g_KstarNLambdastar = 3.18;//from -3.18 to 3.18 
  // double g_KstarNLambdastar = 0.;//from -3.18 to 3.18 
  // double g_KstarNLambdastar = - 3.18;//from -3.18 to 3.18 
  TComplex Mex = TComplex(Mkstarc, -Wkstarc/2.); // :below Eq.(8)
  // TComplex Mex = TComplex(Mkstarc, 0); // :below Eq.(8)
  /* amplitude calculation */
  TComplex amp;
  Mat B;
  double A = g_KstarNLambdastar * g_gammaKKstar;
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      for (int k = 0; k < 4; k++) {
        for (int l = 0; l < 4; l++) {
          B = B + G[l] * Levi(i,j,k,l) * kk[i] * eps_gamma[j] * qq[k]; 
        }
      }
    }
  }
  double lambda_h = 0.650;//GeV
  double lambda_EM = 0.650;//GeV 
  double sq_mom_kaonstar = sq(tt[1]) + sq(tt[2]) + sq(tt[3]); 
  double sq_mom_gamma = sq(kk[1]) + sq(kk[2])+ sq(kk[3]); 
  double F_KstarNLambdastar = (sq(lambda_h)-sq(Mex)) / (sq(lambda_h) + sq_mom_kaonstar);
  double F_gammaKKstar = sq(lambda_EM) / (sq(lambda_EM) + sq_mom_gamma);
  amp = TComplex( A / (t - sq(Mex)) * barDot(u(pp, mf),G[5] * B * u(p, mi)) * F_KstarNLambdastar * F_gammaKKstar , 0 );
  return amp;
}





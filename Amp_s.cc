#include "spinor_mat.h"

TComplex Amp_s(double Eg, double costh, // Eg: lab, costh: cm
    int hel_gamma, int mi, int mf) {
  TLorentzVector k(0, 0, Eg, Eg), p(0, 0, 0, Mp);  // lab system
  TLorentzVector W = k + p;
  /* lab --> CM */
  k.Boost(-W.BoostVector());
  p.Boost(-W.BoostVector());

  /* momentum for K+ */
  TLorentzVector q;
  double m3 = Mkaon;//charged kaon mass
  double m4 = Mlambdastar;//Lambda(1405) mass
  double pdk = TMath::Sqrt((W.M2()-sq(m3+m4))*(W.M2()-sq(m4-m3)))/(2*W.M()); 
  q.SetXYZM(pdk*TMath::Sqrt(1-sq(costh)), 0, pdk*costh, m3);
  TLorentzVector pp = k + p - q;
  double s = W.M2();
  double t = (k-q).M2();
  /* polarization vector */
  TComplex eps_gamma[4];
  FillPolVector(k, hel_gamma, eps_gamma);

  /* deal with the difference of metric definition */
  double kk[4] = {k(3), k(0), k(1), k(2)};
  double p1[4] = {p(3), p(0), p(1), p(2)};
  double qq[4] = {q(3), q(0), q(1), q(2)};
  double tt[4] = {k(3)-q(3), k(0)-q(0), k(1)-q(1), k(2)-q(2)};

  /* amplitude calculation */
  TComplex amp = TComplex(0,0);
  double g_KNLambdastar = 3.18; 
  double kN = 2.7928; 
  // double kN = 3.928; 
  Mat A, A1, A2;
  Mat k1_sla, p1_sla, eps_sla;
  for (int i = 0; i < 4; i++) {
    k1_sla = k1_sla + G[i] *g[i][i] *kk[i];
  }
// std::cout << "------k1_sla--------" << std::endl;
// k1_sla.disp();
// std::cout << "--------------------" << std::endl;
  for (int i = 0; i < 4; i++) {
    p1_sla = p1_sla + G[i] * g[i][i]*p1[i];
  }
// std::cout << "******p1_sla********" << std::endl;
// p1_sla.disp();
// std::cout << "**************" << std::endl;
  for (int i = 0; i < 4; i++) {
    eps_sla= eps_sla + G[i] *g[i][i] *eps_gamma[i]; 
  }
  A1 = (k1_sla + p1_sla + IM * Mp ) * (charge / ( s - sq(Mp)));
  A2 = (k1_sla + p1_sla + IM * Mp) * k1_sla *(charge * kN /(2.*Mp * ( s - sq(Mp) )));

  //Form Factor
  double lambda_h = 0.650;//GeV
  double lambda_EM = 0.650;//GeV (300 MeV)
  double sq_mom_kaon = sq(tt[1]) + sq(tt[2]) + sq(tt[3]); 
  double sq_mom_gamma = sq(kk[1]) + sq(kk[2])+ sq(kk[3]); 
  double F_KNLambda = (sq(lambda_h)-sq(Mkaon))/(sq(lambda_h) + sq_mom_kaon);
  double F_gammaNN = sq(lambda_EM) / (sq(lambda_EM) + sq_mom_gamma);
  amp =TComplex(0,g_KNLambdastar * barDot(u(pp, mf),(A1 + A2) * eps_sla * u(p, mi)) * F_KNLambda * F_gammaNN);

  return amp;
}

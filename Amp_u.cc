#include "spinor_mat.h"

TComplex Amp_u(double Eg, double costh, // Eg: lab, costh: cm
    int hel_gamma, int mi, int mf) {
  TLorentzVector k(0, 0, Eg, Eg), p(0, 0, 0, Mp);  // lab system
  TLorentzVector W = k + p;
  /* lab --> CM */
  k.Boost(-W.BoostVector());
  p.Boost(-W.BoostVector());

  /* momentum for K+ */
  TLorentzVector q;
  double m3 = Mkaon;//charged kaon mass
  double m4 = Mlambdastar;//Lambda(1405) mass
  double pdk = TMath::Sqrt((W.M2()-sq(m3+m4))*(W.M2()-sq(m4-m3)))/(2*W.M()); 
  q.SetXYZM(pdk*TMath::Sqrt(1-sq(costh)), 0, pdk*costh, m3);

  /* CM --> V rest frame */
  // k.Boost(-q.BoostVector());
  // p.Boost(-q.BoostVector());
  // q.Boost(-q.BoostVector());
  // 
  TLorentzVector pp = k + p - q; // mom4 in lab
  double s = W.M2();
  double t = (k-q).M2();
  double U = (pp-k).M2();
  /* polarization vector */
  TComplex eps_gamma[4];
  FillPolVector(k, hel_gamma, eps_gamma);

  /* deal with the difference of metric definition */
  double kk[4] = {k(3), k(0), k(1), k(2)};
  double p1[4] = {p(3), p(0), p(1), p(2)};
  double qq[4] = {q(3), q(0), q(1), q(2)};
double p2[4] = {pp(3), pp(0), pp(1), pp(2)};
  /* amplitude calculation */
  TComplex amp;
  Mat matrixElement_ij;
  TComplex A_ij;
  double g_KNLambdastar = 3.18; 
  double kLstar =0.4;
 double A = charge * g_KNLambdastar / (2. * Mlambdastar); 
  Mat A1;
  Mat k1_sla, p2_sla, eps_sla;
  for (int i = 0; i < 4; i++) {
    k1_sla = k1_sla + G[i]*g[i][i] *kk[i];
  }
  for (int i = 0; i < 4; i++) {
    p2_sla = p2_sla + G[i]*g[i][i]*p2[i];
  }
  for (int i = 0; i < 4; i++) {
    eps_sla= eps_sla + G[i] * g[i][i] * eps_gamma[i]; 
  }
  A1 = eps_sla * kLstar * k1_sla *(p2_sla - k1_sla + IM * Mlambdastar ) * (1./ (U - sq(Mlambdastar)));

  //Form Factor
  double lambda_h = 0.650;//GeV
  double lambda_EM = 0.300;//GeV (300 MeV)
  double sq_mom_kaon = sq(qq[1]) + sq(qq[2]) + sq(qq[3]); 
  double sq_mom_gamma = sq(kk[1]) + sq(kk[2])+ sq(kk[3]); 
  double F_KNLambdastar = (sq(lambda_h)-sq(Mkaon))/(sq(lambda_h) + sq_mom_kaon);
  double F_gammaLL = sq(lambda_EM) / (sq(lambda_EM) + sq_mom_gamma);
  // amp =TComplex(0, -1. * A * barDot(u(pp, mf), A1 * u(p, mi)) );
  amp =TComplex(0, -1. * A * barDot(u(pp, mf), A1 * u(p, mi)) * F_KNLambdastar * F_gammaLL);

  return amp;
}





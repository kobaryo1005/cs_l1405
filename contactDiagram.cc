/* based on arXiv:hep-ph/0602112v4 6 May 2006 */
#include "spinor_mat.h"
#include "formfactor.h"

Mat matrixElement_contact(int i, int j,
                          TLorentzVector k, TLorentzVector p, TLorentzVector q) {
  /* coupling constant */
  /* g_{K* N \Lambda} */
  double const_g = -4.26; // -4.26 (NSC97a), -6.11 (NSC97f) :Eq.(6)
  /* \kappa_{K* N \Lambda} */
  double const_k = 2.66; // 2.66 (NSC97a), 2.43 (NSC97f) :Eq.(6)
  
  /* matrix element calculation */
  Mat matrixElement;
  double A = charge/(2 * Mp);
  matrixElement = sigmaMat[i][j] * (TComplex(0, -1)* A * const_g * const_k);

  /* Form Factor */
  double FF = FormFactor_com(k,p,q);
  
  return matrixElement * sq(FF);
}







